# fresh-theme

A gnome-shell theme.

Based on the shell theme from the Arctic Apricity suite (https://github.com/agajews/ArcticApricity), with minor readability and compatibility tweaks for newer versions of GNOME (3.32+)

---

Extract to `~/.themes/fresh/`